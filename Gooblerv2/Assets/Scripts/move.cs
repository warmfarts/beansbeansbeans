﻿using UnityEngine;
using System.Collections;

public class move : MonoBehaviour
{
    public float speed = 6.0F;
    public float turnSpeed = 45.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;

    private Vector3 moveDirection = Vector3.zero;
    private Animator anim;
    private float vert;
    private bool jumping = false;

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();

        if (controller.isGrounded)
        {
            jumping = false;
            transform.Rotate(0, Input.GetAxis("Horizontal") * turnSpeed * Time.deltaTime, 0);
            moveDirection = transform.forward * Input.GetAxis("Vertical") * speed * 3;

            vert = Input.GetAxis("Vertical");
            anim.SetFloat("move", vert);
            

            if (Input.GetButton("Jump"))
            {
                jumping = true;
                moveDirection.y = jumpSpeed;
            }

        }
        anim.SetBool("jumped", jumping);

        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);

    }
}